# ETournament

El propósito de la aplicación es gestionar las planillas de partidos de waterpolo donde se  registran todos los datos de los equipos involucrados, así como también los eventos que se presentan durante el mismo. Entre los datos básicos del partido están los nombres y números de los jugadores, técnico, y árbitros, y dentro de los eventos del partido están los goles, expulsiones, etc. 


Para ejecutar la aplicación se puede utilizar Docker y Docker Compose

Primero se debe buildear la imagen:

    ./mvnw verify -Pprod dockerfile:build

Luego ejecutar:

    docker-compose -f src/main/docker/app.yml up -d
