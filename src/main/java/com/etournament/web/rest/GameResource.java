package com.etournament.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.etournament.service.GameService;
import com.etournament.service.dto.EventDTO;
import com.etournament.service.dto.FullGameDTO;
import com.etournament.service.dto.GameDTO;
import com.etournament.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller de Partido
 */
@RestController
@RequestMapping("/api")
public class GameResource {

	private final Logger log = LoggerFactory.getLogger(GameResource.class);

	private final GameService gameService;

	public GameResource(GameService gameService) {
		this.gameService = gameService;
	}

	/**
	 * POST /games : Crea un nuevo partido
	 * 
	 * @param gameDTO
	 * @return
	 * @throws URISyntaxException
	 */
	@PostMapping("/games")
	public ResponseEntity<FullGameDTO> createGame(@RequestBody GameDTO gameDTO) throws URISyntaxException {
		log.debug("Invocacion para crear un partido: {}", gameDTO);
		if (gameDTO.getId() != null) {
			throw new BadRequestAlertException("Un nuevo partido no puede tener id", "game", "idexists");
		}
		FullGameDTO result = gameService.addGame(gameDTO);
		return ResponseEntity
				.created(new URI("/api/games/" + result.getId()))
				.body(result);
	}

	/**
	 * PUT /games : Actualiza un partido
	 *
	 * 
	 * @param gameDTO
	 * @return
	 * @throws URISyntaxException
	 */
	@PutMapping("/games")
	public ResponseEntity<FullGameDTO> updateGame(@RequestBody GameDTO gameDTO) throws URISyntaxException {
		log.debug("Invocacion para actualizar un partido: {}", gameDTO);
		if (gameDTO.getId() == null) {
			return createGame(gameDTO);
		}
		FullGameDTO result = gameService.updateGame(gameDTO);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * GET /games : Recupera todos los partidos
	 *
	 * @return 
	 */
	@GetMapping("/games")
	public List<GameDTO> getAllGames() {
		log.debug("Invocación para recuperar todos los partidos");
		return gameService.findAll();
	}

	/**
	 * GET /games/:id : Recupera el partido con "id"
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/games/{id}")
	public ResponseEntity<GameDTO> getGame(@PathVariable String id) {
		log.debug("Invocación para recuperar el partido {}", id);
		GameDTO gameDTO = gameService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(gameDTO));
	}

	/**
	 * DELETE /games/:id : delete the "id" game.
	 *
	 * @param id
	 *            the id of the gameDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/games/{id}")
	public ResponseEntity<Void> deleteGame(@PathVariable String id) {
		log.debug("Invocacion para eliminar partido {}", id);
		gameService.delete(id);
		return ResponseEntity.ok().build();
	}

	/**
	 * POST /games/:gameId/events : Agrega un evento al partido
	 *
	 * @param gameId
	 * @param eventDTO
	 * @return
	 * @throws URISyntaxException
	 */
	@PostMapping("/games/{gameId}/events")
	public ResponseEntity<FullGameDTO> addEvent(@PathVariable String gameId, @RequestBody EventDTO eventDTO)
			throws URISyntaxException {
		log.debug("Invocacion para agregar evento : {}", eventDTO);
		FullGameDTO result = gameService.addEvent(gameId, eventDTO);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * DELETE /games/:gameId/events/:eventId : Elimina un evento del partido
	 * 
	 * @param gameId
	 * @param eventId
	 * @return
	 */
	@DeleteMapping("/games/{gameId}/events/{eventId}")
	public ResponseEntity<FullGameDTO> deleteGame(@PathVariable String gameId, @PathVariable String eventId) {
		log.debug("Invocación para eliminar evento {} del partido {}", eventId, gameId);
		FullGameDTO result = gameService.deleteEvent(gameId, eventId);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * PUT /games/:gameId/stage/next : Avanza el partido a la siguiente etapa
	 * 
	 * @param gameDTO
	 * @return
	 * @throws URISyntaxException
	 */
	@PutMapping("/games/{gameId}/stage/next")
	public ResponseEntity<FullGameDTO> nextStage(@PathVariable String gameId) {
		log.debug("Invocación para avanzar el partido a la siguiente etapa : {}", gameId);
		FullGameDTO result = gameService.nextStage(gameId);
		return ResponseEntity.ok().body(result);
	}

	/**
	 * PUT /games/:gameId/stage/prev: Avanza el partido a la siguiente etapa
	 * 
	 * @param gameDTO
	 * @return
	 * @throws URISyntaxException
	 */
	@PutMapping("/games/{gameId}/stage/previous")
	public ResponseEntity<FullGameDTO> previousStage(@PathVariable String gameId) {
		log.debug("Invocación para retroceder el partido a la etapa anterior: {}", gameId);
		FullGameDTO result = gameService.previousStage(gameId);
		return ResponseEntity.ok().body(result);
	}

}
