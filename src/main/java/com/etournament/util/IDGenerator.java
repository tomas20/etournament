package com.etournament.util;

import java.util.UUID;

/**
 * Esta clase se utiliza para generar numeros aleatorios para asignarlos como
 * identificadores de los objetos.
 * 
 */
public class IDGenerator {

	private IDGenerator() {
	}
	/**
	 * Genera un numero aleatorio 
	 * 
	 * @return un long en base a un numero aleatorio 
	 */
	public static String getId() {
		return UUID.randomUUID().toString().replace("-", "");
	}

}
