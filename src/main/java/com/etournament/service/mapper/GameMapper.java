package com.etournament.service.mapper;

import java.util.Collection;
import java.util.stream.Collectors;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.etournament.domain.Game;
import com.etournament.domain.Player;
import com.etournament.service.dto.FullGameDTO;
import com.etournament.service.dto.GameDTO;
import com.etournament.service.dto.PlayerDTO;

/**
 * Mapper para la entidad {@link Game}
 */
@Mapper(componentModel = "spring", uses = {EventMapper.class})
public abstract  class GameMapper {
	/**
	 * Convierte un {@link GameDTO} en un {@link Game} utilizando el id del DTO si es distinto de null, en caso contrario un nuevo ID generado
	 * No se copian los cambios en stage y los eventos 
	 * 
	 * @param gameDTO
	 * @return
	 */
	@Mapping(target = "id", expression = "java(gameDTO.getId() != null ? gameDTO.getId() : com.etournament.util.IDGenerator.getId())")
	@Mapping(target = "events", ignore = true)
	@Mapping(target = "stage", ignore = true)
	public abstract Game toEntity(GameDTO gameDTO);

	/**
	 * Actualiza un {@link Game} con los datos de un {@link GameDTO}
	 * 
	 * @param game
	 * @param gameDTO
	 */
	public void merge(@MappingTarget Game game, GameDTO gameDTO) {
		baseMerge(game, gameDTO);
		mergePlayers(game, gameDTO.getPlayers());
	}
	
	@Mapping(target = "events", ignore = true)
	@Mapping(target = "players", ignore = true)
	@Mapping(target = "stage", ignore = true)
	protected abstract void baseMerge(@MappingTarget Game game, GameDTO gameDTO);


	/**
	 * Actualiza los Jugadores de un partido en base a una colección de {@link PlayerDTO}
	 * 
	 * @param game
	 * @param playerDTOs
	 */
	private void mergePlayers(Game game, Collection<PlayerDTO> playerDTOs) {

		Collection<Player> playersToDelete = game.getPlayers().stream()
				.filter(player -> playerDTOs.stream().noneMatch(playerDTO -> player.getId().equals(playerDTO.getId())))
				.collect(Collectors.toList());

		game.getPlayers().removeAll(playersToDelete);

		for (PlayerDTO playerDTO : playerDTOs) {
			if (playerDTO.getId() == null) {
				Player player = this.toEntity(playerDTO);
				game.addPlayer(player);
			} else {
				game.getPlayers().stream()
						.filter(player -> player.getId().equals(playerDTO.getId()))
						.findAny()
						.ifPresent(p -> mergePlayer(p, playerDTO));
			}
		}
	}

	@Mapping(target = "id", ignore = true)
	public abstract void mergePlayer(@MappingTarget Player player, PlayerDTO playerDTO);
	
	/**
	 * Crea un {@link PlayerDTO} en base a un {@link Player}
	 * @param player
	 * @return
	 */
	public abstract PlayerDTO toDto(Player player);

	/**
	 * Crea un {@link Player} en base a un {@link PlayerDTO}
	 * @param playerDTO
	 * @return
	 */
	@Mapping(target = "id", expression = "java(playerDTO.getId() != null ? playerDTO.getId() : com.etournament.util.IDGenerator.getId())")
	public abstract Player toEntity(PlayerDTO playerDTO);
	
	/**
	 * Crea un {@link GameDTO} en base a {@link Game} 
	 * 
	 * @param game
	 * @return
	 */
	public abstract GameDTO toGameDTO(Game game);
	
	/**
	 * Crea un {@link FullGameDTO} en base a {@link Game} 
	 * 
	 * @param game
	 * @return
	 */
	public abstract FullGameDTO toFullGameDTO(Game game);


}
