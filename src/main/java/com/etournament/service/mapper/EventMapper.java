package com.etournament.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import com.etournament.domain.Event;
import com.etournament.service.dto.EventDTO;
import com.etournament.service.dto.FullEventDTO;

/**
 * Mapper para la entidad {@link Event} y {@link EventDTO}
 */
@Mapper(componentModel = "spring")
public interface EventMapper {

	/**
	 * Crea un {@link EventDTO} a partir de un {@link Event}
	 * @param event
	 * @return
	 */
    FullEventDTO toFullEventDto(Event event);

    /**
     * Crea un {@link Event} a partir de un {@link EventDTO}
     * @param eventDTO
     * @return
     */
    @Mapping(target = "id",
    		expression = "java(com.etournament.util.IDGenerator.getId())")
    Event toEntity(EventDTO eventDTO);
    
}
