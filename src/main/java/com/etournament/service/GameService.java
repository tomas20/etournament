package com.etournament.service;

import java.util.List;

import com.etournament.service.dto.EventDTO;
import com.etournament.service.dto.FullEventDTO;
import com.etournament.service.dto.FullGameDTO;
import com.etournament.service.dto.GameDTO;

/**
 * Interfaz del servicio Partido
 */
public interface GameService {

	/**
	 * Crea un nuevo partido
	 *
	 * @param gameDTO
	 *            a crear
	 * @return el partido actualizado
	 */
	FullGameDTO addGame(GameDTO gameDTO);

	/**
	 * Actualiza un partido
	 *
	 * @param gameDTO
	 *            a actualizar
	 * @return el partido actualizado
	 */
	FullGameDTO updateGame(GameDTO gameDTO);

	/**
	 * Obtiene todos los partidos
	 *
	 * @return la lista de partidos
	 */
	List<GameDTO> findAll();

	/**
	 * Obtiene el juego "id"
	 *
	 * @param id
	 *            del partido
	 * @return el partido
	 */
	FullGameDTO findOne(String id);

	/**
	 * Elimina el partido con "id"
	 *
	 * @param id
	 */
	void delete(String id);

	/**
	 * Guarda un evento al partido
	 *
	 * @param gameId
	 * @param event
	 * @return El evento guardado
	 */
	FullGameDTO addEvent(String gameId, EventDTO event);

	/**
	 * Elimina un evento
	 *
	 * @param id
	 *            del evento
	 */
	FullGameDTO deleteEvent(String gameId, String eventId);

	/**
	 * Avanza el partido a la siguiente etapa
	 *
	 * @return el Partido actualizado
	 */
	FullGameDTO nextStage(String gameId);

	/**
	 * Vuelve el partido a la etapa anterior
	 *
	 * @return el Partido actualizado
	 */
	FullGameDTO previousStage(String gameId);

}
