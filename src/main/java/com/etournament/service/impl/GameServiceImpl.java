package com.etournament.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.etournament.domain.Game;
import com.etournament.domain.Tournament;
import com.etournament.repository.GameRepository;
import com.etournament.repository.TournamentRepository;
import com.etournament.service.GameService;
import com.etournament.service.dto.EventDTO;
import com.etournament.service.dto.FullGameDTO;
import com.etournament.service.dto.GameDTO;
import com.etournament.service.mapper.EventMapper;
import com.etournament.service.mapper.GameMapper;

/**
 * Implementacion del servicio {@link GameService}
 */
@Service
@Transactional
public class GameServiceImpl implements GameService {

    private final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    @Autowired private TournamentRepository tournamentRepository;
    @Autowired private GameRepository gameRepository;
    @Autowired private GameMapper gameMapper;
    @Autowired private EventMapper eventMapper;

    @Override
    public FullGameDTO addGame(GameDTO gameDTO) {
        Game game = gameMapper.toEntity(gameDTO);
        Tournament tournament = tournamentRepository.findOne(Tournament.DEFAULT_ID);
        return gameMapper.toFullGameDTO(tournament.addGame(game));
    }

    @Override
    public FullGameDTO updateGame(GameDTO gameDTO) {
        Game game = gameRepository.findOne(gameDTO.getId());
        gameMapper.merge(game, gameDTO);
        return gameMapper.toFullGameDTO(game);
    }
    
    @Override
    @Transactional(readOnly = true)
    public List<GameDTO> findAll() {
        log.debug("Request to get all Games");
        return gameRepository.findAll().stream()
            .map(gameMapper::toGameDTO)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public FullGameDTO findOne(String id) {
        log.debug("Request to get Game : {}", id);
        Game game = gameRepository.findOne(id);
        return gameMapper.toFullGameDTO(game);
    }

    @Override
    public void delete(String id) {
        log.debug("Request to delete Game : {}", id);
        gameRepository.delete(id);
    }
    
	@Override
	public FullGameDTO addEvent(String gameId, EventDTO eventDTO) {
        log.debug("Agregando un evento al partido: {}", gameId);
		Game game = gameRepository.findOne(gameId);
		Assert.isTrue(game != null, "El partido no existe");
		game.addEvent(eventMapper.toEntity(eventDTO));
		return gameMapper.toFullGameDTO(game);
	}

	@Override
	public FullGameDTO deleteEvent(String gameId, String eventId) {
		log.debug("Eliminando el evento {} al partido: {}", eventId, gameId);
		Game game = gameRepository.findOne(gameId);
		Assert.isTrue(game != null, "El partido no existe");
		game.removeEvent(eventId);		
		return gameMapper.toFullGameDTO(game);
	}

	@Override
	public FullGameDTO nextStage(String gameId) {
		Game game = gameRepository.findOne(gameId);
		Assert.isTrue(game != null, "El partido no existe");
		game.nextStage();
		return gameMapper.toFullGameDTO(game);
	}

	@Override
	public FullGameDTO previousStage(String gameId) {
		Game game = gameRepository.findOne(gameId);
		Assert.isTrue(game != null, "El partido no existe");
		game.previousStage();
		return gameMapper.toFullGameDTO(game);
	}

}
