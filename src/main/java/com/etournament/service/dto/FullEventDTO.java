package com.etournament.service.dto;


import lombok.Data;

/**
 * DTO completo para la entidad Event
 */
@Data
public class FullEventDTO extends EventDTO {
	
	private static final long serialVersionUID = 6139845308931126081L;

	private String id;
	
    private Integer quarter;
}
