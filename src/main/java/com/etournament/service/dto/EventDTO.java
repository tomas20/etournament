package com.etournament.service.dto;


import java.io.Serializable;

import com.etournament.domain.enumeration.EventType;
import com.etournament.domain.enumeration.TeamEnum;

import lombok.Data;

/**
 * DTO base para la entidad Event
 */
@Data
public class EventDTO implements Serializable {
	
	private static final long serialVersionUID = 6139845308931126081L;

    private EventType type;

    private Integer playerNumer;

    private TeamEnum team;

    private String time;

}
