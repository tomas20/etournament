package com.etournament.service.dto;


import java.io.Serializable;
import java.util.List;

import com.etournament.domain.Game;
import com.etournament.domain.enumeration.GameStage;

import lombok.Data;

/**
 * DTO base de {@link Game}
 */
@Data
public class GameDTO implements Serializable {

	private static final long serialVersionUID = -1178452283840540420L;

	private String id;

    private String refereeName;

    private String refereeInform;

    private GameStage stage;

    private String blueTeamName;

    private String blueCouchName;

    private String whiteTeamName;

    private List<PlayerDTO> players;

}
