package com.etournament.service.dto;


import java.util.List;

import com.etournament.domain.Event;

import lombok.Data;

/**
 * A DTO for the Game entity.
 */
@Data
public class FullGameDTO extends GameDTO {

	private static final long serialVersionUID = -8005279865624225790L;

    private Integer blueGoals;

    private Integer whiteGoals;
    
    private List<EventDTO> events;
}
