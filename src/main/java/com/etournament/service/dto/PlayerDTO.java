package com.etournament.service.dto;


import java.io.Serializable;

import com.etournament.domain.enumeration.TeamEnum;

import lombok.Data;

/**
 * DTO para la entidad Player
 */
@Data
public class PlayerDTO implements Serializable {

	private static final long serialVersionUID = -4829894200364954729L;

	private String id;

    private String name;

    private Integer number;

    private TeamEnum team;

}
