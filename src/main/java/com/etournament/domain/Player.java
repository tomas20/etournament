package com.etournament.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.etournament.domain.enumeration.TeamEnum;
import com.etournament.util.IDGenerator;

import lombok.Data;

/**
 * A Player.
 */
@Entity
@Table(name = "player")
@Data
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "player_number")
    private Integer number;

    @Enumerated(EnumType.STRING)
    @Column(name = "team")
    private TeamEnum team;
    
    public Player() {
    	this.id = IDGenerator.getId();
    }

}
