package com.etournament.domain;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.etournament.domain.enumeration.EventType;
import com.etournament.domain.enumeration.GameStage;
import com.etournament.domain.enumeration.TeamEnum;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * A Game.
 */
@Entity
@Table(name = "game")
@Data
public class Game implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	private String id;

	@Column(name = "referee_name")
	private String refereeName;

	@Enumerated(EnumType.STRING)
	@Column(name = "stage")
	private GameStage stage;

	@Column(name = "blue_team_name")
	private String blueTeamName;

	@Column(name = "blue_couch_name")
	private String blueCouchName;

	@Column(name = "blue_goals")
	private Integer blueGoals;

	@Column(name = "white_team_name")
	private String whiteTeamName;

	@Column(name = "white_couch_name")
	private String whiteCouchName;

	@Column(name = "white_goals")
	private Integer whiteGoals;

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "game_id")
	@JsonIgnore
	private Collection<Event> events = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name = "game_id")
	@JsonIgnore
	private Collection<Player> players = new HashSet<>();

	public Game() {
		this.stage = GameStage.PROGRAMMED;
		this.whiteGoals = 0;
		this.blueGoals = 0;
	}

	/**
	 * Agrega un player al Partido
	 * 
	 * @param player
	 */
	public Player addPlayer(Player player) {
		this.players.add(player);
		return player;
	}

	/**
	 * Agrega un evento al partido
	 * 
	 * @param event
	 */
	public Event addEvent(Event event) {

		if (this.getStage().isQuarter()) {
			events.add(event);
			if (EventType.GOAL.equals(event.getType())) {
				updateScore();
			}
			event.setQuarter(stage.getQuarter());
			event.setGame(this);
		} else {
			throw new IllegalArgumentException("Los eventos solo pueden registrarse en tiempo de juego");
		}
		return event;
	}

	/**
	 * Elimina un evento del partido a partir de su ID
	 * 
	 * @param eventId
	 */
	public void removeEvent(String eventId) {
		Optional<Event> event = this.events.stream().filter(e -> e.getId().equals(eventId)).findFirst();
		if (event.isPresent()) {
			this.removeEvent(event.get());
		} else {
			throw new IllegalArgumentException("No existe el evento en el partido");
		}
	}

	/**
	 * Elimina un Evento del partido
	 * 
	 * @param event
	 */
	private void removeEvent(Event event) {
		this.events.remove(event);
		if (EventType.GOAL.equals(event.getType())) {
			updateScore();
		}
	}

	/**
	 * Actualiza el resultado del partido
	 */
	protected synchronized void updateScore() {
		Integer blueGoalsTemp = 0, whiteGoalsTemp = 0;
		for (Event e : events) {
			if (EventType.GOAL.equals(e.getType())) {
				if (TeamEnum.BLUE.equals(e.getTeam())) {
					blueGoalsTemp++;
				} else {
					whiteGoalsTemp++;
				}
			}
		}
		this.blueGoals = blueGoalsTemp;
		this.whiteGoals = whiteGoalsTemp;
	}

	/**
	 * Avanza el partido a la próxima etapa
	 */
	public void nextStage() {
		if (this.stage.nextStage() == null) {
			throw new IllegalArgumentException("El partido ya se encuentra en su última etapa");
		} else {
			this.stage = this.stage.nextStage();
		}
	}

	/**
	 * Retrocede el partido a la etapa anterior
	 */
	public void previousStage() {
		if (this.stage.previousStage() == null) {
			throw new IllegalArgumentException("El partido ya se encuentra en su primer etapa");
		} else {
			this.stage = this.stage.previousStage();
		}
	}

}
