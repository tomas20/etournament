package com.etournament.domain.enumeration;

/**
 * The TeamEnum enumeration.
 */
public enum TeamEnum {
    BLUE, WHITE
}
