package com.etournament.domain.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    GOAL, 
    SIMPLE_EXPULSION, 
    CHANGE_EXPULSION, 
    BRUTALITY_EXPULSION, 
    PENALTY,
    TIME_OUT
}
