package com.etournament.domain.enumeration;

import lombok.Getter;

/**
 * The GameStage enumeration.
 */
@Getter
public enum GameStage {
	
	PROGRAMMED(1, -1), 
	FIRST_QUARTER(2, 1), 
	FIRST_TIME_OUT(3, -1), 
	SECOND_QUARTER(4, 2), 
	SECOND_TIME_OUT(5, -1), 
	THIRD_QUARTER(6, 3), 
	THIRD_TIME_OUT(7, -1), 
	FOURTH_QUARTER(8, 4), 
	FINISHED(9, -1);
	
	private Integer order;
	private Integer quarter;

	private GameStage(Integer order, Integer quarter) {
		this.order = order;
		this.quarter = quarter;
	}
	
	/**
	 * @return la etapa anterior del partido
	 */
	public GameStage previousStage() {
		return stageByOrder(this.getOrder() - 1);
	}
	
	/**
	 * @return La proxima etapa del partido
	 */
	public GameStage nextStage() {
		return stageByOrder(this.getOrder() + 1);
	}
	
	/**
	 * Obtiene una etapa segun el orden 
	 * @param order de la etapa a obtener
	 * @return etapa en el order
	 */
	private GameStage stageByOrder(Integer order) {
		GameStage stage = null;
		for (GameStage s : GameStage.values()) {
			if (s.getOrder().equals(order)) {
				stage = s;
				break;
			}
		}
		return stage;
	}
	
	/**
	 * @return true si es un tiempo de juego
	 */
	public boolean isQuarter() {
		return this.quarter != -1;
	}
}
