/**
 * Author: Tomás Antonioli
 * Date:  20 jun. 2018 - 18:40:30
 */
package com.etournament.domain;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author Tomás Antonioli <tomas@antonioli.com.ar>
 *
 */
@Entity
@Table(name = "tournament")
public class Tournament {

		
	public final static String DEFAULT_ID = "1";
	
	@Id
	@Column(name = "id")
	private String id;
	
	@OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinColumn(name="tournament_id")
	private Collection<Game> games;
	
	public Game addGame(Game game) {
		games.add(game);
		return game;
	}
	
	
}
