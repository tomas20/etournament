package com.etournament.domain;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.etournament.domain.enumeration.EventType;
import com.etournament.domain.enumeration.TeamEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.Data;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
@Data
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id")
    private String id;

    @Enumerated(EnumType.STRING)
    @Column(name = "event_type")
    private EventType type;

    @Column(name = "player_numer")
    private Integer playerNumer;

    @Enumerated(EnumType.STRING)
    @Column(name = "team")
    private TeamEnum team;

    @Column(name = "time")
    private String time;

    @Column(name = "quarter")
    private Integer quarter;

    @ManyToOne
    @JsonBackReference
    private Game game;
    
}
