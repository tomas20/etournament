package com.etournament.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.etournament.domain.Tournament;

/**
 *  Repositorio de {@link Tournament}
 */
@Repository
public interface TournamentRepository extends JpaRepository<Tournament, String>{
	

}
